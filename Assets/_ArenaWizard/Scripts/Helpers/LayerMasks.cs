﻿using UnityEngine;

public static class LayerMasks 
{
	//public static int PlayerLayerMask = LayerMask.GetMask("Player") + 1;
	public static int MouseRaycastTargetLayerMask = LayerMask.GetMask("Terrain") + 1;

	public static int PlayerSingleLayerMask = LayerMask.NameToLayer("Player");
	public static int EnemySingleLayerMask = LayerMask.NameToLayer("Enemy");
}
