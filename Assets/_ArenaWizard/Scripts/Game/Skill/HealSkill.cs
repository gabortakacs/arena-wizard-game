﻿using UnityEngine;

public abstract class HealSkill : Skill 
{

	[SerializeField] protected float _healAmount;

	public void DoBurstHeal(IHealable target)
	{
		target.TakeHeal(_healAmount);
	}

	protected void DoDotHeal(IHealable target)
	{
		target.TakeHeal(_healAmount / _duration * Time.deltaTime);
	}




}

