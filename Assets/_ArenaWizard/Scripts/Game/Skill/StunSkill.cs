﻿using UnityEngine;

public class StunSkill : Skill 
{
	[SerializeField] private float _speed;

	private Vector3 _direction;

	public Vector3 Direction { set { _direction = value; } }

	private void Update()
	{
		Vector3 pos = transform.position;
		pos += _direction * _speed * Time.deltaTime;
		transform.position = pos;
	}

	protected void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == _skillOwner)
			return;

		IStunable target = other.GetComponent<IStunable>();
		if (target != null)
		{
			Stun(target);
		}
		PushBackToPoll(0);
	}

	public void Stun(IStunable target)
	{
		target.Stunned(_duration);
	}

	public override void Reset()
	{
		base.Reset();
		_direction = Vector3.zero;
	}
}

