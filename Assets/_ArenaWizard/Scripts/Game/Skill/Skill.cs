﻿using System;
using System.Collections;
using UnityEngine;

public enum SkillType
{
	BurstSingleTarget,
	DotSingleTarget,
	BurstAoe,
	DotAoe,
	AuraDmg,
	Stun,
	HealSelfBurst,
	HealSelfDot
}

public abstract class Skill : ObjectPoolEntity 
{
	[SerializeField] private float _cooldownTime;
	[SerializeField] protected float _duration;
	[SerializeField] protected SkillType _type;
	[SerializeField] private Sprite _icon;

	private Coroutine _pushBackRoutine;

	protected GameObject _skillOwner;

	public float CoolDownTime { get { return _cooldownTime; } }
	public float Duration { get { return _duration; } }
	public SkillType Type { get { return _type; } }
	public GameObject SkillOwner { set { _skillOwner = value; } }
	public Sprite Icon { get { return _icon; } }

	public void PushBackToPoll(float delay)
	{
		if (_pushBackRoutine != null)
			StopCoroutine(_pushBackRoutine);
		if (delay == 0)
			_owner.Push(this);
		else
			_pushBackRoutine = StartCoroutine(PushBackRoutine(delay));
	}

	IEnumerator PushBackRoutine(float delay)
	{
		yield return new WaitForSeconds(delay);
		_owner.Push(this);
	}

	public override void Reset()
	{
		_skillOwner = null;	
	}
}

