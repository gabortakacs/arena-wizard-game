﻿using UnityEngine;

public class DamageSkillSingle : DamageSkill 
{
	[SerializeField] private float _speed;

	private Transform _targetTransform;
	private IDamageable _targetIDamageable;
	private bool _isTargeted = false;
	private Vector3 _direction;

	private Collider[] _colliders;

	public Vector3 Direction { set { _direction = value; } }

	private void Awake()
	{
		if(_type == SkillType.DotSingleTarget)
			_colliders = GetComponentsInChildren<Collider>();
	}

	private void Update()
	{
		if(_isTargeted)
		{
			transform.position = _targetTransform.position;
			DoDotDamage(_targetIDamageable);
		}
		else
		{
			Vector3 pos = transform.position;
			pos += _direction * _speed * Time.deltaTime;
			transform.position = pos;
		}
	}

	protected void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == _skillOwner || _skillOwner.layer == LayerMasks.EnemySingleLayerMask && other.gameObject.layer == LayerMasks.EnemySingleLayerMask)
			return;

		IDamageable target = other.GetComponent<IDamageable>();
		if (target == null)
		{
			PushBackToPoll(0);
			return;
		}
		if (_type == SkillType.BurstSingleTarget)
		{
			DoBurstDamage(target);
			PushBackToPoll(0);
		}
		else if(_type == SkillType.DotSingleTarget)
		{
			_targetTransform = other.transform;
			_targetIDamageable = target;
			_isTargeted = true;
			TurnCollidersEnabled(false);
			PushBackToPoll(_duration-Time.deltaTime);
		}
	}

	public override void Reset()
	{
		base.Reset();
		_isTargeted = false;
		_direction = Vector3.zero;
		_targetTransform = null;
		_targetIDamageable = null;
		if (_type == SkillType.DotSingleTarget)
			TurnCollidersEnabled(true);
	}

	private void TurnCollidersEnabled(bool isEnabled)
	{
		foreach (Collider collider in _colliders)
		{
			collider.enabled = isEnabled;
		}
	}
}

