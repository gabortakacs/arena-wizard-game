﻿using System.Collections.Generic;
using UnityEngine;

public abstract class SkillController : MonoBehaviour 
{


	[SerializeField] protected float _bulletSpawnDistance = 1.0f;
	[SerializeField] protected ObjectPool[] _skillObjectPools;

	protected List<ObjectPool> _skillObjectPoolList;
	protected List<float> _cooldownTimer;
	protected GameObject _owner;

	protected abstract void InitStunSkill(Skill skill, GameObject skillGO);
	protected abstract void InitHealSelfSkill(Skill skill, GameObject skillGO);
	protected abstract void InitSingleTargetSkill(Skill skill, GameObject skillGO);
	protected abstract void InitAoeSkill(Skill skill, GameObject skillGO);


	protected virtual void Awake()
	{
		_cooldownTimer = new List<float>(_skillObjectPools.Length);
		_skillObjectPoolList = new List<ObjectPool>(_skillObjectPools.Length);
		for(int i = 0; i < _skillObjectPools.Length; ++i)
		{
			_cooldownTimer.Add(0);
			_skillObjectPoolList.Add(_skillObjectPools[i]);
		}
		//_cooldownTimer = new float[_skillObjectPools.Length];
	}

	public virtual void AddSkillObjectPool(ObjectPool objectPool)
	{
		_cooldownTimer.Add(0);
		_skillObjectPoolList.Add(objectPool);
	}

	protected virtual Skill UseSkill(int index)
	{
		Skill skill;
		GameObject skillGO;
		PollSkill(index, out skill, out skillGO);
		if (typeof(DamageSkillSingle) == skill.GetType())
		{
			InitSingleTargetSkill(skill, skillGO);
		}
		else if (typeof(DamageSkillAoe) == skill.GetType())
		{
			InitAoeSkill(skill, skillGO);
		}
		else if (typeof(DamageSkillAura) == skill.GetType())
		{
			InitAuraSkill(skill, skillGO);
		}
		else if (typeof(HealSkillSelf) == skill.GetType())
		{
			InitHealSelfSkill(skill, skillGO);
		}
		else if (typeof(StunSkill) == skill.GetType())
		{
			InitStunSkill(skill, skillGO);
		}
		_cooldownTimer[index] = Time.time + skill.CoolDownTime;
		return skill;
	}

	protected virtual void InitAuraSkill(Skill skill, GameObject skillGO)
	{
		skillGO.transform.position = _owner.transform.position;
		((DamageSkillAura)skill).Target = _owner.transform;
		skill.PushBackToPoll(skill.Duration);
	}

	protected void PollSkill(int index, out Skill skill, out GameObject skillGO)
	{
		skill = (Skill)_skillObjectPoolList[index].Poll();
		skillGO = skill.gameObject;
		skillGO.SetActive(true);
		skill.SkillOwner = _owner;
	}
}

