﻿using UnityEngine;

public abstract class DamageSkill : Skill 
{
	[SerializeField] protected float _damage;

	protected void DoBurstDamage(IDamageable target)
	{
		target.TakeDamage(_damage);
	}

	protected void DoDotDamage(IDamageable target)
	{
		target.TakeDamage(_damage / _duration * Time.deltaTime);
	}
}

