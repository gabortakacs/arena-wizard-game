﻿using UnityEngine;

public class DamageSkillAoe : DamageSkill
{
	protected void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == _skillOwner || _skillOwner.layer == LayerMasks.EnemySingleLayerMask && other.gameObject.layer == LayerMasks.EnemySingleLayerMask)
			return;

		IDamageable target = other.GetComponent<IDamageable>();
		if (target == null)
			return;

		if (_type == SkillType.BurstAoe)
			DoBurstDamage(target);
	}

	protected void OnTriggerStay(Collider other)
	{
		if (other.gameObject == _skillOwner || _skillOwner.layer == LayerMasks.EnemySingleLayerMask && other.gameObject.layer == LayerMasks.EnemySingleLayerMask)
			return;

		IDamageable target = other.GetComponent<IDamageable>();
		if (target == null)
			return;

		if (_type == SkillType.DotAoe || _type == SkillType.AuraDmg)
			DoDotDamage(target);
	}
}

