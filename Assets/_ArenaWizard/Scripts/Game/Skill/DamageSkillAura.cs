﻿using UnityEngine;

public class DamageSkillAura : DamageSkillAoe 
{

	private Transform _target;

	public Transform Target { set { _target = value; } }

	private void Update()
	{
		transform.position = _target.position;
	}

	public override void Reset()
	{
		base.Reset();
		_target = null;
	}
}

