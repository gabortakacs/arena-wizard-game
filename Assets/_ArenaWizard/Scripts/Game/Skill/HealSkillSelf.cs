﻿using UnityEngine;

public class HealSkillSelf : HealSkill 
{

	private Transform _positionTarget;
	private IHealable _healTarget;

	public Transform PositionTarget { set { _positionTarget = value; } }
	public IHealable HealTarget { set { _healTarget = value; } }

	private void Update()
	{
		DoDotHeal(_healTarget);
		transform.position = _positionTarget.position;
	}

	public override void Reset()
	{
		_positionTarget = null;
		_healTarget = null;
	}
}
