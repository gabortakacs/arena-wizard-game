﻿using System.Collections;
using UnityEngine;

public delegate void PlayerHPChanged(float amount);
public delegate void ArmorChanged(float amount);

public class Player : Entity, IHealable, IStunable
{
	[SerializeField] PlayerMouseTargeting _playerMouseTargeting;

	private float _vertical;
	private float _horizontal;
	private bool _isStunned = false;
	private Coroutine _stunnedRoutine;

	public bool IsStunned { get { return _isStunned; } }
	public PlayerMouseTargeting PlayerMouseTargeting { get { return _playerMouseTargeting; } }
	public event PlayerHPChanged OnPlayerHPChanged;
	public event ArmorChanged OnArmorChanged;


	private void Update()
	{
		if (Input.GetButton("Mouse 1"))
		{
			Move();
		}
	}

	public override void Die()
	{
		GameMaster.Instance.GameOver();
	}

	protected override void ChangeHP(float amount)
	{
		base.ChangeHP(amount);
		if (OnPlayerHPChanged != null)
			OnPlayerHPChanged.Invoke(amount);
	}

	protected override void DecreaseArmor(float amount)
	{
		base.DecreaseArmor(amount);
		if (OnArmorChanged != null)
			OnArmorChanged.Invoke(amount);
	}

	public override void Move()
	{
		_navAgent.SetDestination(_playerMouseTargeting.TargetSign.position);
		//float speed = _speed * Time.deltaTime;
		//Vector3 dir = TargetSi
		//transform.position = new Vector3(transform.position.x +  _horizontal * speed, transform.position.y, transform.position.z + _vertical * speed);
	}

	public override void Reset()
	{
	}

	public void TakeHeal(float amount)
	{
		ChangeHP(amount);
	}

	//private void ReadMoveInputs()
	//{
	//	_horizontal = Input.GetAxis("Horizontal");
	//	_vertical = Input.GetAxis("Vertical");
	//}

	public void Stunned(float duration)
	{
		if (_stunnedRoutine != null)
			StopCoroutine(_stunnedRoutine);
		_stunnedRoutine = StartCoroutine(StunnedRoutine(duration));
	}

	IEnumerator StunnedRoutine(float duration)
	{
		_isStunned = true;
		_navAgent.isStopped = true;
		yield return new WaitForSeconds(duration);
		_isStunned = false;
		_navAgent.isStopped = false;
	}
}

