﻿using System;
using System.Collections;
using UnityEngine;

public delegate void SkillUsed(int index, float cooldown);

public class PlayerSkillController : SkillController
{
	[SerializeField] Player _player;

	public event SkillUsed OnSkillUsed;
	public Sprite[] SkillIconSprites
	{
		get
		{
			Sprite[] sprites = new Sprite[_skillObjectPools.Length];
			for(int i = 0; i < sprites.Length; ++i)
			{
				Skill tmp = (Skill)_skillObjectPools[i].Poll();
				sprites[i] = tmp.Icon;
				_skillObjectPools[i].Push(tmp);
			}
			return sprites;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		_owner = _player.gameObject;
	}

	private void Update()
	{
		for (int i = 0; i < _skillObjectPools.Length; ++i)
		{
			if (Input.GetButton("Fire" + (i + 1).ToString()))
			{
				if (_cooldownTimer[i] < Time.time)
				{
					UseSkill(i);
				}
			}
		}
	}

	protected override Skill UseSkill(int index)
	{
		Skill skill = base.UseSkill(index);
		if (skill == null)
			return null;
		SkillUsed(index, skill.CoolDownTime);
		return skill;
	}

	protected override void InitStunSkill(Skill skill, GameObject skillGO)
	{
		skillGO.transform.rotation = Quaternion.identity;
		Vector3 direction = (_player.PlayerMouseTargeting.TargetSign.position - transform.position).normalized;
		direction.y = 0;
		((StunSkill)skill).Direction = direction;
		skillGO.transform.position = transform.position + direction * _bulletSpawnDistance;
		skill.PushBackToPoll(60.0f);
	}

	protected override void InitHealSelfSkill(Skill skill, GameObject skillGO)
	{
		HealSkillSelf healSelf = (HealSkillSelf)skill;
		if (skill.Type == SkillType.HealSelfBurst)
		{
			healSelf.DoBurstHeal(_player);
			skill.PushBackToPoll(0.0f);
		}
		else
		{
			skillGO.transform.position = _player.transform.position;
			healSelf.HealTarget = _player;
			healSelf.PositionTarget = _player.transform;
			skill.PushBackToPoll(skill.Duration);
		}
	}

	protected override void InitSingleTargetSkill(Skill skill, GameObject skillGO)
	{
		skillGO.transform.rotation = Quaternion.identity;
		Vector3 direction = (_player.PlayerMouseTargeting.TargetSign.position - transform.position).normalized;
		direction.y = 0;
		((DamageSkillSingle)skill).Direction = direction;
		skillGO.transform.position = transform.position + direction * _bulletSpawnDistance;
		skill.PushBackToPoll(60.0f);
	}

	protected override void InitAoeSkill(Skill skill, GameObject skillGO)
	{
		skillGO.transform.position = _player.PlayerMouseTargeting.TargetSign.position;
		skill.PushBackToPoll(skill.Duration);
	}

	public void SkillUsed(int index, float cooldown)
	{
		if (OnSkillUsed != null)
			OnSkillUsed.Invoke(index, cooldown);
	}
}

