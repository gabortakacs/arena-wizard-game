﻿using UnityEngine;

public class PlayerMouseTargeting : MonoBehaviour
{
	public Transform _targetSign;
	public Transform TargetSign { get { return _targetSign; } set {_targetSign = value; }}

	private void Update()
	{
		TargetSignToMouse();
	}

	void TargetSignToMouse()
	{
		if (_targetSign == null)
			return;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 1000, LayerMasks.MouseRaycastTargetLayerMask))
		{
			_targetSign.transform.position = hit.point;
		}
	}


}

