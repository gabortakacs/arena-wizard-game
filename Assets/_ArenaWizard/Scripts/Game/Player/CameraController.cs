﻿using UnityEngine;

public class CameraController : MonoBehaviour 
{
	[SerializeField] private Player _player;
	[SerializeField] private float _delayTimeSpeedRate = 10f;

	private float _startDistanceX;
	private float _startDistanceZ;

	private Vector3 _cameraVelocity;

	private void Start()
	{
		_startDistanceX = transform.position.x - _player.transform.position.x;
		_startDistanceZ = transform.position.z - _player.transform.position.z ;
	}

	private void Update()
	{
		Vector3 _nextPosition = _player.transform.position;
		_nextPosition.x += _startDistanceX;
		_nextPosition.y = transform.position.y;
		_nextPosition.z += _startDistanceZ;
		transform.position = Vector3.SmoothDamp(transform.position, _nextPosition, ref _cameraVelocity, _delayTimeSpeedRate / _player.Speed);
	}
}
