﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyRangedAttackSensor : MonoBehaviour 
{
	[SerializeField] EnemySkillController _enemySkillContoller;
	[SerializeField] private int[] indexes;

	private void OnTriggerEnter(Collider other)
	{
		SetSkillsInRange(true);
	}

	private void OnTriggerExit(Collider other)
	{
		SetSkillsInRange(false);
	}

	private void SetSkillsInRange(bool newValue)
	{
		for (int i = 0; i < indexes.Length; ++i)
		{
			_enemySkillContoller.SetSkillInRange(i, newValue);
		}
	}
}

