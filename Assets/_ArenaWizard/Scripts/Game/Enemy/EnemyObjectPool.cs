﻿using UnityEngine;

public class EnemyObjectPool : ObjectPool 
{
	[SerializeField] SpawnPlaces _spawnPlaces;
	[SerializeField] ObjectPool _skill;

	protected override void Awake()
	{
		for (int i = 0; i < _amount; ++i)
		{
			ObjectPoolEntity tmp = Instantiate(_entityPrefab);
			tmp.gameObject.transform.parent = transform;
			tmp.gameObject.SetActive(false);
			tmp.Owner = this;
			EnemySkillController enemyTmp = tmp.GetComponent<EnemySkillController>();
			enemyTmp.AddSkillObjectPool(_skill);
			//enemyTmp.s
			_pool.Enqueue(tmp);
		}
	}

	public override ObjectPoolEntity Poll()
	{
		ObjectPoolEntity obj = base.Poll();
		obj.transform.position = _spawnPlaces.GetRandomSpawnPlace().position;
		return obj;
	}

}

