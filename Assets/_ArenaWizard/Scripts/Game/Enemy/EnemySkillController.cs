﻿using System.Collections.Generic;
using UnityEngine;

public class EnemySkillController : SkillController
{
	[SerializeField] private Enemy _enemyOwner;
	protected List<bool> _skillTargetInRange;
	
	protected override void Awake()
	{
		base.Awake();
		_owner = gameObject;
		_skillTargetInRange = new List<bool>(_skillObjectPools.Length);
		for(int i = 0; i < _skillObjectPools.Length; ++i)
		{
			_skillTargetInRange.Add(false);
		}
	}

	protected void Update()
	{
		if (_enemyOwner.IsStunned)
			return;
		for (int i = 0; i < _cooldownTimer.Count; ++i)
		{
			if (_skillTargetInRange[i] && _cooldownTimer[i] <= Time.time)
			{
				UseSkill(i);
			}
		}
	}

	public override void AddSkillObjectPool(ObjectPool objectPool)
	{
		base.AddSkillObjectPool(objectPool);
		_skillTargetInRange.Add(false);
	}

	public void SetSkillInRange(int index, bool inRange)
	{
		_skillTargetInRange[index] = inRange;
	}

	protected override void InitAoeSkill(Skill skill, GameObject skillGO)
	{
		throw new System.NotImplementedException();
	}

	protected override void InitHealSelfSkill(Skill skill, GameObject skillGO)
	{
		throw new System.NotImplementedException();
	}

	protected override void InitSingleTargetSkill(Skill skill, GameObject skillGO)
	{
		skillGO.transform.rotation = Quaternion.identity;
		Vector3 direction = (_enemyOwner.Target.gameObject.transform.position - transform.position).normalized;
		direction.y = 0;
		((DamageSkillSingle)skill).Direction = direction;
		skillGO.transform.position = transform.position + direction * _bulletSpawnDistance;
		skill.PushBackToPoll(60.0f);
	}

	protected override void InitStunSkill(Skill skill, GameObject skillGO)
	{
		throw new System.NotImplementedException();
	}
}

