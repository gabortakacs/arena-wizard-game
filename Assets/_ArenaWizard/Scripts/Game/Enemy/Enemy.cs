﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(SkillController))]
public class Enemy : Entity, IStunable
{
	[SerializeField] private Player _player;
	//[SerializeField] private float _range;

	private Vector3 _prevDestination;
	private bool _isStunned = false;
	private Coroutine _stunnedRoutine;

	public bool IsStunned { get { return _isStunned; } }
	public Player Target { get{ return _player; } }

	protected override void Awake()
	{
		base.Awake();
	}

	private void Start()
	{
		_player = FindObjectOfType<Player>();
	}
	private void Update()
	{
		Move();
	}

	public override void Die()
	{
		Debug.Log("DIED");
		_owner.Push(this);
	}

	public override void Move()
	{
		//if (_isStunned)
		//{
		//	_navAgent.isStopped = true;
		//	return;
		//}
		//_navAgent.isStopped = false;
		Vector3 targetPos = _player.transform.position;
		if (_prevDestination != targetPos || _navAgent.pathStatus == NavMeshPathStatus.PathInvalid || _navAgent.isPathStale)
		{
			_navAgent.SetDestination(targetPos);
			_prevDestination = targetPos;
		}
	}

	public void Stunned(float duration)
	{
		if (_stunnedRoutine != null)
			StopCoroutine(_stunnedRoutine);
		_stunnedRoutine = StartCoroutine(StunnedRoutine(duration));
	}

	IEnumerator StunnedRoutine(float duration)
	{
		_isStunned = true;
		_navAgent.isStopped = true;
		yield return new WaitForSeconds(duration);
		_isStunned = false;
		_navAgent.isStopped = false;
	}

	public override void Reset()
	{
		_isStunned = false;
		if (_stunnedRoutine != null)
			StopCoroutine(_stunnedRoutine);
		_health = _maxHealth;
	}
}

