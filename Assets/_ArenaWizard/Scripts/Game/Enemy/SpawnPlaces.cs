﻿using UnityEngine;

public class SpawnPlaces : MonoBehaviour 
{
	[SerializeField] private Transform[] _spawnPlaces;

	public Transform GetRandomSpawnPlace()
	{
		return _spawnPlaces[Random.Range(0, _spawnPlaces.Length)];
	}
}

