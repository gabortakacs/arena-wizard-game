﻿using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class Entity : ObjectPoolEntity, IDamageable
{
	[SerializeField] protected NavMeshAgent _navAgent;
	[SerializeField] protected float _health = 100;
	[SerializeField] private float _speed = 4;
	[SerializeField] protected float _armor = 10.0f;
	[SerializeField] private float _armorDestructionRate = 0.1f;

	protected float _maxHealth;
	protected float _maxArmor;

	public float Health { get { return _health; } }
	public float MaxHealth { get { return _maxHealth; } }
	public float MaxArmor { get { return _maxArmor; } }
	public float Speed { get { return _speed; } set { _speed = value; _navAgent.speed = _speed; } }
	public float Armor { get { return _armor; } set { _armor = value; } }

	public abstract void Die();
	public abstract void Move();

	protected virtual void Awake()
	{
		_navAgent.speed = _speed;
		_maxHealth = _health;
		_maxArmor = _armor;
	}

	public virtual void TakeDamage(float amount)
	{
		float value = _armor > 0 ? -amount / _armor : -amount;
		ChangeHP(value);
		DecreaseArmor(-value);
	}

	protected virtual void DecreaseArmor(float amount)
	{
		Debug.Log(_armor + "->" + amount + "->" + (_armor-amount* _armorDestructionRate));
		_armor -= amount * _armorDestructionRate;
		if (_armor < 0)
			_armor = 0;
	}

	protected virtual void ChangeHP(float amount)
	{
		_health += amount;
		if (_health < 0)
			Die();
		if (_health > _maxHealth)
			_health = _maxHealth;
	}

}

