﻿using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawnManager : Singleton<EnemyObjectPool> 
{
	[SerializeField] private EnemyObjectPool[] _enemyObjectPools;
	[SerializeField] private int[] _enemySpawnChances;
	[SerializeField] private float _enemySpawnTime = 10;
	[SerializeField] private int _startEnemyAmount = 10;

	private int[] _enemySpawnChanceNumbers;
	private float _timer;

	private void Awake()
	{
		_enemySpawnChanceNumbers = new int[_enemySpawnChances.Length];
		int sum = 0;
		for (int i = 0; i < _enemySpawnChances.Length; ++i)
		{
			sum += _enemySpawnChances[i];
			_enemySpawnChanceNumbers[i] = sum;
		}
	}

	private void Start()
	{
		for(int i = 0; i < _startEnemyAmount; ++i)
		{
			RespawnEnemy();
		}
	}

	private void Update()
	{
		if(_timer < Time.time)
		{
			RespawnEnemy();
			_timer = Time.time + _enemySpawnTime;
		}
	}

	private void RespawnEnemy()
	{
		int random = Random.Range(0, _enemySpawnChanceNumbers[_enemySpawnChanceNumbers.Length-1]);
		int index = 0;
		for(; index <_enemySpawnChanceNumbers.Length; ++index)
		{
			if(_enemySpawnChanceNumbers[index] > random)
				break;
		}
		ObjectPoolEntity enemy = _enemyObjectPools[index].Poll();
		enemy.gameObject.SetActive(true);
	}
}

