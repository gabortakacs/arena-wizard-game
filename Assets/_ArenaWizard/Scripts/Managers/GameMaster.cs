﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : Singleton<GameMaster> {

	[SerializeField] private float _normalTimeScale = 1.0f;
	[SerializeField] private float _pausedTimeScale = 0.000001f;
	[SerializeField] private int _mainMenuBuildIndex = 0;
	[SerializeField] private int _endlessSceneBuildIndex = 1;

	private bool _isPaused = false;
	private bool _isGameOver = false;

	public bool IsPaused { get { return _isPaused; } }
	public bool IsGameOver { get { return _isGameOver; } }

	private void Awake()
	{
		Time.timeScale = _normalTimeScale;
	}

	public void GameOver()
	{
		MenuMaster.Instance.GameOverMenu.SetActive(true);
		_isGameOver = true;
	}

	public void OnPlayerDied()
	{
		GameOver();
	}

	public void StartGame()
	{
		LoadScene(_endlessSceneBuildIndex);
	}

	public void BackToMainMenu()
	{
		LoadScene(_mainMenuBuildIndex);
	}

	public void LoadScene(int index)
	{
		SceneManager.LoadScene(index);
	}

	public void RestartScene()
	{
		LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void QuitGame()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	public void Pause()
	{
		_isPaused = true;
		Time.timeScale = _pausedTimeScale;
	}

	public void Resume()
	{
		Time.timeScale = _normalTimeScale;
		_isPaused = false;
	}

	public void Continue()
	{
		throw new NotImplementedException();
	}

	public void PlayerDie()
	{
		GameOver();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
	}
}
