﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuMaster : Singleton<MenuMaster>
{
	[SerializeField] private MenuPage _startMenu;
	[SerializeField] private MenuPage _pauseMenu;
	[SerializeField] private MenuPage _gameOverMenu;
	[SerializeField] private MenuPage _optionsMenu;
	[SerializeField] private Toggle _soundToogle;
	[SerializeField] private GameObject _playerHUD;
	[SerializeField] private bool _startMenuOnStart = false;

	public GameObject GameOverMenu { get { return _gameOverMenu.gameObject; } }

	public void Start()
	{
		if (_startMenuOnStart)
		{
			SetMenuPageActive(_startMenu);
		}
		if(_soundToogle)
		{
			_soundToogle.isOn = SoundMaster.Instance.SoundEnabled;
		}
	}

	public void Update()
	{
		if (Input.GetButtonDown("Cancel") && !GameMaster.Instance.IsGameOver && !_startMenuOnStart)
		{
			if (!GameMaster.Instance.IsPaused)
				Pause();
			else
				Resume();
		}
	}

	public void StartGame()
	{
		GameMaster.Instance.StartGame();
	}

	public void Resume()
	{
		DeativeAllPages();
		GameMaster.Instance.Resume();
	}
	
	public void Continue()
	{
		GameMaster.Instance.Continue();
	}

	public void QuitGame()
	{
		GameMaster.Instance.QuitGame();
	}

	public void Pause()
	{
		SetMenuPageActive(_pauseMenu);
		GameMaster.Instance.Pause();
	}

	public void Restart()
	{
		GameMaster.Instance.RestartScene();
	}

	public void LoadMainMenuScene()
	{
		GameMaster.Instance.BackToMainMenu();
	}

	public void Options()
	{
		SetMenuPageActive(_optionsMenu);
	}

	public void BackToMainMenu()
	{
		MenuPage nextMenu = null;
		if (_startMenuOnStart)
		{
			nextMenu = _startMenu;
		}
		else if (GameMaster.Instance.IsPaused)
		{
			nextMenu = _pauseMenu;
		}
		else if(GameMaster.Instance.IsGameOver)
		{
			nextMenu = _gameOverMenu;
		}
		SetMenuPageActive(nextMenu);
	}

	public void SoundToogleSwitched(bool toState)
	{
		SoundMaster.Instance.SoundEnabled = toState;
	}

	private void SetMenuPageActive(MenuPage page)
	{
		DeativeAllPages();
		page.gameObject.SetActive(true);
	}

	private void DeativeAllPages()
	{
		if (_gameOverMenu)
			_gameOverMenu.gameObject.SetActive(false);

		if(_optionsMenu)
			_optionsMenu.gameObject.SetActive(false);

		if (_pauseMenu)
			_pauseMenu.gameObject.SetActive(false);

		if (_playerHUD)
			_pauseMenu.gameObject.SetActive(false);

		if (_startMenu)
			_startMenu.gameObject.SetActive(false);
	}
}
