﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUDContoller : MonoBehaviour 
{
	[SerializeField] private Player _player;

	[Header("Healthbar")]
	[SerializeField] private Slider _healthbarSlider;
	[SerializeField] private Gradient _healthbarColor;

	[Header("Armorbar")]
	[SerializeField] private Slider _armorbarSlider;
	[SerializeField] private Gradient _armorbarColor;

	[Header("Skillbar")]
	[SerializeField] private PlayerSkillController _playerSkillController;
	[SerializeField] private Transform _skillIconParent;
	[SerializeField] private int _iconStartPosX = 10;
	[SerializeField] private int _offsetBetweenIcons = 60;
	[SerializeField] private Image _skillIconPrefab;
	[SerializeField] private Image _onCDIconPrefab;
	[SerializeField] private Text _onCDIconTextPrefab;

	private Image[] _skillIcons;
	private Image[] _onCDIcons;
	private Text[] _onCDTexts;
	public Image _healthbarSliderImage;
	public Image _armorbarSliderImage;

	private void Start()
	{
		_playerSkillController.OnSkillUsed += OnSkillUsed;
		_player.OnPlayerHPChanged += OnPlayerHPChanged;
		_player.OnArmorChanged += OnArmorChanged;

		_healthbarSliderImage = _healthbarSlider.fillRect.GetComponent<Image>();
		_armorbarSliderImage = _armorbarSlider.fillRect.GetComponent<Image>();

		CreateSkillbarIcons();
		OnPlayerHPChanged(0);
		OnArmorChanged(0);
	}

	private void OnPlayerHPChanged(float amount)
	{
		_healthbarSlider.value = _player.Health / _player.MaxHealth;
		_healthbarSliderImage.color = _healthbarColor.Evaluate(_healthbarSlider.value);
	}
	private void OnArmorChanged(float amount)
	{
		_armorbarSlider.value = _player.Armor / _player.MaxArmor;
		_armorbarSliderImage.color = _armorbarColor.Evaluate(_armorbarSlider.value);
	}

	private void OnSkillUsed(int index, float cooldown)
	{
		StartCoroutine(SkillOnCD(index, cooldown));
	}

	private void CreateSkillbarIcons()
	{
		Sprite[] _skillIconSprites = _playerSkillController.SkillIconSprites;
		_skillIcons = new Image[_skillIconSprites.Length];
		_onCDIcons = new Image[_skillIconSprites.Length];
		_onCDTexts = new Text[_skillIconSprites.Length];
		for (int i = 0; i < _skillIconSprites.Length; ++i)
		{
			_skillIcons[i] = Instantiate(_skillIconPrefab, _skillIconParent);
			_skillIcons[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(_iconStartPosX + i * _offsetBetweenIcons, 0.0f);
			_skillIcons[i].sprite = _skillIconSprites[i];
			_onCDIcons[i] = Instantiate(_onCDIconPrefab, _skillIcons[i].transform);
			_onCDIcons[i].transform.position = _skillIcons[i].transform.position;
			_onCDTexts[i] = Instantiate(_onCDIconTextPrefab, _skillIcons[i].transform);
			_onCDTexts[i].text = "";
			_onCDIcons[i].fillAmount = 0.0f;
		}
	}

	IEnumerator SkillOnCD(int index, float cooldown)
	{
		float timer = cooldown;
		while(timer > 0)
		{
			_onCDTexts[index].text = timer >= 1.0 ? timer.ToString("F0") : timer.ToString("F1");
			_onCDIcons[index].fillAmount = timer / cooldown;
			timer -= Time.deltaTime;
			yield return null;
		}
		_onCDTexts[index].text = "";
		_onCDIcons[index].fillAmount = 0.0f;
	}

	private void OnDestroy()
	{
		if (_playerSkillController != null)
			_playerSkillController.OnSkillUsed -= OnSkillUsed;
		if (_player != null)
		{
			_player.OnPlayerHPChanged -= OnPlayerHPChanged;
			_player.OnArmorChanged -= OnArmorChanged;
		}
	}
}

